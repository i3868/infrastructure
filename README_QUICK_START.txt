Make sure you are in swarm mode:
docker swarm init

To deploy infra:
docker stack deploy -c .\main-stack.yml main
docker stack deploy -c .\portainer-stack.yml portainer

To delete stacks:
docker stack rm -c main
docker stack rm -c portainer

To leave the swarm:
docker swarm leave

To pull images authenticate tu  gitlab registry:
docker login registry.gitlab.com

To build your local image:
docker build -t registry.gitlab.com/i3868/<image> <dockerfile_path>

To add your own pipeline runner:

https://ocw.cs.pub.ro/courses/idp/laboratoare/06 -> Gitlab Runners
The token used for registration can be found here https://gitlab.com/groups/i3868/-/runners
Use idp, frontend, backend for tags